<?php

namespace Gmi\Testimonial\Model;

use Magento\Framework\Model\AbstractModel;

class Testimonial extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Gmi\Testimonial\Model\ResourceModel\Testimonial');
    }
}
