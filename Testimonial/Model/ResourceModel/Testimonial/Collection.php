<?php

namespace Gmi\Testimonial\Model\ResourceModel\Testimonial;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'testimonial_id';
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Gmi\Testimonial\Model\Testimonial',
            'Gmi\Testimonial\Model\ResourceModel\Testimonial'
        );
    }
}
