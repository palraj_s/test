<?php

namespace Gmi\Testimonial\Block;

use Magento\Framework\View\Element\Template\Context;
use Gmi\Testimonial\Model\TestimonialFactory;
/**
 * Testimonial List block
 */
class TestimonialListData extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Testimonial
     */
    protected $_testimonial;
    public function __construct(
        Context $context,
        TestimonialFactory $testimonial
    ) {
        $this->_testimonial = $testimonial;
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Testimonial'));
        
        if ($this->getTestimonialCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'gmi.testimonial.pager'
            )->setAvailableLimit(array(5=>5,10=>10,15=>15))->setShowPerPage(true)->setCollection(
                $this->getTestimonialCollection()
            );
            $this->setChild('pager', $pager);
            $this->getTestimonialCollection()->load();
        }
        return parent::_prepareLayout();
    }

    public function getTestimonialCollection()
    {
        $page = ($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 5;

        $testimonial = $this->_testimonial->create();
        $collection = $testimonial->getCollection();
        $collection->addFieldToFilter('status','1');
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
