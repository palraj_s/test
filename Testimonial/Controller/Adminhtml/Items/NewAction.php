<?php


namespace Gmi\Testimonial\Controller\Adminhtml\Items;

class NewAction extends \Gmi\Testimonial\Controller\Adminhtml\Items
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
