require(['jquery'],function($){
	
	var carousel = function() {
		
//testimonial carousel
	
		$('.carousel-testimony').owlCarousel({
			center: true,
			loop: false,
			items:1,
			margin: 30,
			stagePadding: 0,
			nav: false,
			navText: ['<span class="ion-ios-arrow-back">', '<span class="ion-ios-arrow-forward">'],
			responsive:{
				0:{
					items: 1
				},
				600:{
					items: 3
				},
				1000:{
					items: 3
				}
			}
		});

	};
	carousel();
	
});
